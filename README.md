[Syntaxis](https://syntaxis.com.pl/) has introduced [micromodules](https://syntaxis.com.pl/en/29-micromodules) which are basically pre-built modules that can replace entire modules from a modular synth (for example, VCA, ADSR, VCO, VCF). While the concept itself is awesome, hooking it up to your existing setup can be a challenge. 
Recently they started offering faceplates and PCB's to allow you to convert individual micromodules into eurorack modules. Unfortunately that does take up as much space as regular eurorack modules which kind of defeats the purpose for me. This is why I started designing a custom set of PCB's that allow me to combine some of the micromodules together into a single eurorack module. 

![uADSR module](/images/uadsr.png)
![Front panel impression](/images/fp_var1.png)
![uGlue PCB impression](/images/uglue.png)

## Scope of this module

The goal is to combine the following syntaxis micromodules into a single module:

* [uADSR-3310-A](https://syntaxis.com.pl/en/micromodules/80-uadsr-3310-a.html) (voltage controlled ADSR micromodule)
* [uVCO-3340-A](https://syntaxis.com.pl/en/micromodules/77-uvco-3340-a.html) (voltage controlled oscillator micromodule)
* [uVCF-2044-LP4-A](https://syntaxis.com.pl/en/micromodules/109-uvcf-2044-lp4-a.html) (voltage controlled filter micromodule)

## Structure of the module

View from the top:
```
|     |      |  < micromodules
|     |      |
==============  < uGlue PCB
      ||        < 2.54 pitch header that connects uGlue to the front panel PCB
       |        < Front panel PCB (where all pots are mounted to)
  [   ]|[   ]   < Potmeters mounted on the front panel PCB
==============  < Front panel (which is the thing you see mounted in the Eurorack)
    |     |     < Potmeter shafts
```

The 3.5mm jacks are mounted to their own PCB which will connect to the Front panel PCB with wires (also view from the top):
```
============== < Jack PCB
[  ] [  ] [  ] < Jacks 
============== < Front panel (which is the thing you see mounted in the Eurorack)
```

### Requirements: 

* The module will allow for two CV inputs to be fed to the uVCO (allowing for an LFO signal to added for example)
* The uVCO module is optional. When an external VCO output is fed into the VCO jack the internal uVCO will be ignored.
* The Attack, Decay, Sustain and Release knobs will be available on the front panel and connected to the uVCF
* A resonance knob will be available on the front panel and connected to the uVCF
* A fine tuning knob will be available and connected to the uVCO
* All three signals that the uVCO generates (pulse, saw, triangle) are available on individual jacks
* A switch on the front panel allows selection of the uVCO signal to send to the uVCF
* The uGlue board exposes most pins that are present on the modules to the front panel PCB (so we can potentially support different front panel designs in the future)

## Folder structure

* `.`: The root folder contains the eagle files (board + schematics)
* `lib`: Contains the eagle library that I maintain solely for this project. You don't need this unless you'll be working on uGlue PCB's
* `images`: contains image files that show off the current state of the PCB designs. The names of the files correspond to the eagle file names. Also includes images of the micromodules to show in the readme.
* `gerber`: contains gerber files for the individual PCBs. I try to update them on every commit but if you want to be sure, generate them yourself :) 
